# -*- coding: utf-8 -*-
import re
from datetime import datetime, timedelta

from image_ticket import make_ticket
from settings import FLIGHTS


# 1 Город отправления
def handler_departure_cities(text, context):
    text = text.title()
    for flight in FLIGHTS:
        match = re.match(r'^(\w{0,4})(\w+)_', flight)
        if match.group(1) in text:
            context['departure_city'] = match.group(1) + match.group(2)
            return True
    else:
        return False


# 2 Город назначения
def handler_destination_city(text, context):
    text = text.title()
    for flight in FLIGHTS:
        match = re.match(context["departure_city"] + r'_(\w{0,4})(\w+)$', flight)
        if match:
            if match.group(1) in text:
                context['destination_city'] = match.group(1) + match.group(2)
                context['flight'] = f"{context['departure_city']}_{context['destination_city']}"
                return True
    else:
        return False


# 3 Дата
def handler_date(text, context):
    try:
        date = datetime.strptime(text, '%d-%m-%Y')
        if datetime.now() < date:
            next_flights(date, context)
            return True
        else:
            return False
    except ValueError as err:
        return False


# 4 Ближайшие рейсы
def handler_nearest_flights(text, context):
    if text.isdigit():
        if '1' <= text <= '5':
            context['like_flight'] = context[text]
            return True
    return False


# 5 Количиство мест
def handler_seats(text, context):
    if text.isdigit():
        text = int(text)
        if 1 <= text <= 5:
            context['seats'] = text
            return True
    return False


# 6 Коментарий
def handler_comment(text, context):
    context['comments'] = text
    return True


# 7 Потверждение данных
def handler_confirmation(text, context):
    text = text.lower()
    if text == 'да' or text == 'нет':
        context['confirmation'] = text
        return True
    else:
        return False


# 8 Телефон
def handler_phone(text, context):
    match = re.match(r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$', text)
    if match:
        context['phone'] = text
        return True
    else:
        return False


def next_flights(date, context):
    flight = FLIGHTS[context['flight']]
    periodicity = flight['periodicity']
    next_date_flights = []
    if periodicity == "month":
        for day in flight['days']:
            if date.day < day:
                next_date_flights.append(f'{day}.{date.month}.{date.year} в {flight["time"]}')
                if len(next_date_flights) > 5:
                    for index, date_cur_flight in enumerate(next_date_flights):
                        context[str(index + 1)] = date_cur_flight
                        return
        else:
            for index, date_cur_flight in enumerate(next_date_flights):
                if index == 5:
                    return
                context[str(index + 1)] = date_cur_flight
    else:
        first_dates_to_weeks(date, context, flight, next_date_flights)
        date += timedelta(weeks=1)
        while len(next_date_flights) < 5:
            first_dates_to_weeks(date, context, flight, next_date_flights, True)
            date += timedelta(weeks=1)
        for index, date_cur_flight in enumerate(next_date_flights):
            context[str(index + 1)] = date_cur_flight


def first_dates_to_weeks(date, flight, next_date_flights, is_not_first=False):
    cur_weekday = date.isoweekday()
    for day in flight['days']:
        if cur_weekday < day or is_not_first:
            difference = day - cur_weekday
            temp = date + timedelta(days=difference)
            next_date_flights.append(f'{temp.day}.{temp.month}.{temp.year} в {flight["time"]}')


def handler_generate_ticket(text, context):
    return make_ticket(
        context['fullname'],
        context['departure_city'],
        context['destination_city'],
        context['like_flight'][0:5]
    )
