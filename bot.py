# -*- coding: utf8 -*-
import re

import requests
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id

import handlers
import settings
from models import UserState, db_session, Ticket
from my_token import CUR_TOKEN as TOKEN, ID_GROUP
import vk_api
import logging

log = logging.getLogger("bot")


def configure_logging():
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter("%(levelname)s %(message)s"))
    stream_handler.setLevel(logging.DEBUG)
    log.addHandler(stream_handler)

    file_handler = logging.FileHandler("bot.log")
    file_handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(message)s",
                                                "%d-%m-%Y %H:%M"))
    file_handler.setLevel(logging.DEBUG)
    log.addHandler(file_handler)
    log.setLevel(logging.DEBUG)


class Bot:

    def __init__(self, token):
        self.vk_session = vk_api.VkApi(token=token)
        self.vk_api = self.vk_session.get_api()
        self.long_poll = VkBotLongPoll(self.vk_session, ID_GROUP)

    def run(self):
        for event in self.long_poll.listen():
            try:
                self.on_event(event)
            except Exception:
                log.exception("Не удалось обработать событие")

    @db_session
    def on_event(self, event):
        if event.type != VkBotEventType.MESSAGE_NEW:
            log.info(f"Еще не умеем обрабатывать такое событие|{event.type}")
            return

        text = event.object.message['text']
        user_id = event.object.message['peer_id']

        state = UserState.get(user_id=str(user_id))

        # Name
        if state:
            user = self.vk_session.method("users.get", {"user_ids": user_id})
            fullname = user[0]['last_name'] + ' ' + user[0]['first_name']
            state.context['fullname'] = fullname

        # Ищим Интент
        for intent in settings.INTENTS:
            if text in intent['tokens']:
                self.finish_scenario(state)
                if intent['answer']:
                    self.send_text(intent['answer'], user_id)
                else:
                    self.start_scenario(intent['scenario'], user_id, text)
                break
        else:
            if state is not None:
                self.continue_scenario(text, state, user_id)
            else:
                self.send_text(settings.DEFAULT_ANSWER, user_id)

    def send_text(self, text_to_send, user_id):
        log.debug(f"Отправленное сообщение|{text_to_send}")
        self.vk_api.messages.send(
            user_id=user_id,
            random_id=get_random_id(),
            message=text_to_send
        )

    def send_image(self, image, user_id):
        upload_url = self.vk_api.photos.getMessagesUploadServer()['upload_url']
        upload_data = requests.post(url=upload_url, files={'photo': ('image.png', image, 'image/png')}).json()
        image_data = self.vk_api.photos.saveMessagesPhoto(**upload_data)

        owner_id = image_data[0]['owner_id']
        media_id = image_data[0]['id']
        attachment = f'photo{owner_id}_{media_id}'

        self.vk_api.messages.send(
            user_id=user_id,
            random_id=get_random_id(),
            attachment=attachment
        )

    def send_step(self, step, user_id, text, context):
        if 'text' in step:
            self.send_text(step['text'].format(**context), user_id=user_id)
        if 'image' in step:
            handler = getattr(handlers, step['image'])
            image = handler(text, context)
            self.send_image(image, user_id=user_id)

    def continue_scenario(self, text, state, user_id):
        steps = settings.SCENARIOS[state.scenario_name]['steps']
        step = steps[state.step_name]
        context = state.context
        next_step = steps[step['next_step']]

        handler = getattr(handlers, step['handler'])
        if handler(text, context):
            self.next_step_scenario(context, state, step, next_step, text, user_id)
        else:
            self.repeat_step_scenario(state, step, user_id)

    def repeat_step_scenario(self, state, step, user_id):
        if step['handler'] == 'handler_departure_cities':
            cities_with_flights = set()
            for flight in settings.FLIGHTS:
                match = re.match(r'^(\w+)_', flight)
                cities_with_flights.add(match.group(1))
            for city in cities_with_flights:
                self.send_text(f'\t{city}\n', user_id=user_id)
        else:
            self.send_text(step['failure_text'].format(**state.context), user_id=user_id)
            if step['handler'] == 'handler_destination_city':
                self.finish_scenario(state)

    def next_step_scenario(self, context, state, step, next_step, text, user_id):
        if step['handler'] == 'handler_date':
            text_to_send = next_step['text']
            for i in range(1, 6):
                i = str(i)
                if i in context:
                    text_to_send += f"\t{i}. {context[i]}\n"
            self.send_text(text_to_send, user_id=user_id)
        elif step['handler'] == 'handler_confirmation':
            if context['confirmation'].lower() == 'нет':
                self.finish_scenario(state)
            else:
                self.send_step(step=next_step, user_id=user_id, text=text, context=context)
        else:
            self.send_step(step=next_step, user_id=user_id, text=text, context=context)
        if next_step['next_step']:
            state.step_name = step['next_step']
        else:
            self.finish_scenario(state)

    def start_scenario(self, scenario_name, user_id, text):
        scenario = settings.SCENARIOS[scenario_name]
        first_step = scenario['first_step']
        step = scenario['steps'][first_step]
        self.send_step(step, user_id, text, context={})
        UserState(user_id=str(user_id), scenario_name=scenario_name, step_name=first_step, context={})

    def finish_scenario(self, state):
        if state:
            state.delete()
            try:
                Ticket(
                    flight=state.context['flight'],
                    date_flight=state.context['like_flight'],
                    seats=str(state.context['seats']),
                    phone=state.context['phone'],
                    ticket_number=str(state.id),
                    comment=state.context['comments'],
                )
            except Exception as exc:
                pass


# http://joxi.ru/BA0XlVoiPG0dYA - Консоль
# http://joxi.ru/p27NyVPHWMQ9pm - Переписка с ботом
# http://joxi.ru/EA4yxVoSpP9PQr - Лог
if __name__ == '__main__':
    configure_logging()
    bot = Bot(TOKEN)
    bot.run()
# Зачет!
