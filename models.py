# -*- coding: utf-8 -*-
from pony.orm import *

from settings import DB_CONFIG

db = Database()
db.bind(**DB_CONFIG)


class UserState(db.Entity):
    user_id = Required(str, unique=True)
    scenario_name = Required(str)
    step_name = Required(str)
    context = Required(Json)

class Ticket(db.Entity):
    flight = Required(str)
    date_flight = Required(str)
    seats = Required(str)
    phone = Required(str)
    ticket_number = Required(str, unique=True)
    comment = Required(str)

db.generate_mapping(create_tables=True)
