# -*- coding: utf8 -*-
from unittest import TestCase
from unittest.mock import patch, Mock, ANY

from vk_api.bot_longpoll import VkBotMessageEvent

from bot import Bot


class Test1(TestCase):
    RAW_EVENT = {'type': 'message_new',
                 'object': {
                     'message': {'date': 1598425478, 'from_id': 81651341, 'id': 52, 'out': 0, 'peer_id': 81651341,
                                 'text': 's',
                                 'conversation_message_id': 50, 'fwd_messages': [], 'important': False, 'random_id': 0,
                                 'attachments': [], 'is_hidden': False},
                     'client_info': {'button_actions': ['text', 'vkpay', 'open_app', 'location', 'open_link'],
                                     'keyboard': True,
                                     'inline_keyboard': True, 'carousel': False, 'lang_id': 0}}, 'group_id': 197784452,
                 'event_id': 'c3e237a43c63bd5f465b7d2d968d2078d69d426b'}

    def test_run(self):
        count = 5
        obj = {'any': 999}
        events = [obj] * count
        long_poller_mock = Mock(return_value=events)
        long_poller_listen_mock = Mock()
        long_poller_listen_mock.listen = long_poller_mock

        with patch('bot.vk_api.VkApi'):
            with patch('bot.VkBotLongPoll', return_value=long_poller_listen_mock):
                bot = Bot('')
                bot.on_event = Mock()
                bot.run()

                bot.on_event.assert_called()
                bot.on_event.assert_any_call(obj)
                assert bot.on_event.call_count == count

    def test_on_event(self):
        event = VkBotMessageEvent(raw=self.RAW_EVENT)

        send_mock = Mock()
        with patch('bot.vk_api.VkApi'):
            with patch('bot.VkBotLongPoll'):
                bot = Bot('')
                bot.vk_api = Mock()
                bot.vk_api.messages.send = send_mock

                bot.on_event(event)

        send_mock.assert_called_once_with(
            user_id=self.RAW_EVENT['object']['message']['peer_id'],
            random_id=ANY,
            message=self.RAW_EVENT['object']['message']['text']
        )
