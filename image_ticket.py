# -*- coding: utf-8 -*-
from io import BytesIO

import requests
from PIL import Image, ImageDraw, ImageFont, ImageColor

IMAGE_PATH = "assets_ticket/ticket_template.png"
FONT_PATH = "assets_ticket/ofont.ru_Bloknot.ttf"

COLOR = ImageColor.colormap['black']

FIO_OFFSET = (47, 125)
FROM_OFFSET = (47, 195)
TO_OFFSET = (47, 260)
DATE_OFFSET = (285, 260)

AVATAR_SIZE = 80
AVATAR_OFFSET = (430, 100)


def make_ticket(fio, from_, to, date):
    im = Image.open(IMAGE_PATH)
    draw = ImageDraw.Draw(im)
    font = ImageFont.truetype(FONT_PATH, size=14)

    draw.text(FIO_OFFSET, fio.upper(), font=font, fill=COLOR)
    draw.text(FROM_OFFSET, from_.upper(), font=font, fill=COLOR)
    draw.text(TO_OFFSET, to.upper(), font=font, fill=COLOR)
    draw.text(DATE_OFFSET, date.upper(), font=font, fill=COLOR)

    response = requests.get(f"http://avatar.3sd.me/{AVATAR_SIZE}")
    avatar_file_like = BytesIO(response.content)
    avatar = Image.open(avatar_file_like)

    im.paste(avatar, AVATAR_OFFSET)

    temp_file = BytesIO()
    im.save(temp_file, 'png')
    temp_file.seek(0)

    return temp_file


if __name__ == '__main__':
    make_ticket("Бондаренко А.И.", "Новосибирск", "Москва", "09.08")
