# -*- coding: utf-8 -*-
INTENTS = [
    {
        "name": "ticket",
        "tokens": "/ticket билет",
        "scenario": "ordering_tickets",
        "answer": None,
    },
    {
        "name": "help",
        "tokens": "/help",
        "scenario": None,
        "answer": "/ticket — заказ билетов.\n"
                  "/help — выдает справку о том, как работает бот."
    },
]

SCENARIOS = {
    "ordering_tickets": {
        "first_step": "step1",
        "steps": {
            "step1": {
                "text": "Введите город отправления",
                "failure_text": None,
                "handler": "handler_departure_cities",
                "next_step": "step2",
            },
            "step2": {
                "text": "Введите город назначения",
                "failure_text": "Между выбранными городами нет рейса",
                "handler": "handler_destination_city",
                "next_step": "step3",
            },
            "step3": {
                "text": "Введите дату вылета в формате 01-05-2019",
                "failure_text": "Дата введена не коректно. Попробуйте еще раз",
                "handler": "handler_date",
                "next_step": "step4",
            },
            "step4": {
                "text": "Ближайшие рейсы к указанной дате. \nВыбирите понравившийся номер:\n",
                "failure_text": "Введите цифру понравившегося номер",
                "handler": "handler_nearest_flights",
                "next_step": "step5",
            },
            "step5": {
                "text": "Уточните количество мест (от 1 до 5)",
                "failure_text": "Введите цифру от 1 до 5",
                "handler": "handler_seats",
                "next_step": "step6",
            },
            "step6": {
                "text": "Введите коментарий (по желанию, можно оставить пустым)",
                "failure_text": None,
                "handler": "handler_comment",
                "next_step": "step7",
            },
            "step7": {
                "text": "Все данные точны? да/нет\n"
                        "Город отправления - {departure_city}\n"
                        "Город назначения - {destination_city}\n"
                        "Дата отправления - {like_flight}\n"
                        "Кол-во мест - {seats}",
                "failure_text": "Введите 'да' или 'нет'",
                "handler": "handler_confirmation",
                "next_step": "step8",
            },
            "step8": {
                "text": "Введите номер телефона",
                "failure_text": "Не верный формат телефона. Попробуйте еще раз",
                "handler": "handler_phone",
                "next_step": "step9",
            },
            "step9": {
                "text": "С вами свяжутся по введенному номеру. Ваш билет ниже. "
                        "Распечатайте его.",
                "image": "handler_generate_ticket",
                "failure_text": None,
                "handler": None,
                "next_step": None,
            }
        }
    }
}

DEFAULT_ANSWER = 'Попробуйте использовать /help'

FLIGHTS = {
    'Москва_Лондон': {
        'periodicity': 'month',
        'days': [10, 15, 20, 25, 30],
        'time': '15:30'
    },
    'Лондон_Париж': {
        'periodicity': 'month',
        'days': [5, 15, 25],
        'time': '17:10'
    },
    'Москва_Париж': {
        'periodicity': 'month',
        'days': [22, 23, 25],
        'time': '12:00'
    },
    'Москва_Берлин': {
        'periodicity': 'week',
        'days': [1, 5, 7],
        'time': '7:45'
    },
    'Берлин_Чикаго': {
        'periodicity': 'week',
        'days': [1, 2, 4],
        'time': '21:20'
    },
    'Чикаго_Москва': {
        'periodicity': 'month',
        'days': [7, 14, 21, 28],
        'time': '18:32'
    },
    'Париж_Москва': {
        'periodicity': 'week',
        'days': [1, 2, 3, 4, 5, 6, 7],
        'time': '18:32'
    },
    'Париж_Чикаго': {
        'periodicity': 'week',
        'days': [1, 2, 4, 6, 7],
        'time': '18:32'
    },
}

DB_CONFIG = dict(
    provider='postgres',
    user='postgres',
    password='root',
    host='localhost',
    database='vk_chat_bot'
)